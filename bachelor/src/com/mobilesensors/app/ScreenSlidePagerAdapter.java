package com.mobilesensors.app;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.List;

@SuppressLint("DefaultLocale")
public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
	private List<Fragment> fragments;
	protected String[] CONTENT = new String[] { "Camera", "Accelerometer",
			"Magnetometer", "Accelerometer & Magnetometer", "Gyroscope",
			"Rotation Vector", "Combined results",
			"Accelerometer & Magnetometer & Gyroscope", "3D movement",
			"Calculations" };

	public ScreenSlidePagerAdapter(FragmentManager fm,
			List<Fragment> fragments, String[] content) {
		super(fm);
		this.fragments = fragments;
		this.CONTENT = content;
	}

	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return CONTENT[position % CONTENT.length].toUpperCase();
	}

	@Override
	public int getCount() {
		return this.fragments.size();
	}

}
