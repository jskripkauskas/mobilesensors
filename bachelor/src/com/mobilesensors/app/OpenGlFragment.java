package com.mobilesensors.app;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class OpenGlFragment extends Fragment {
	private GLSurfaceView mGLView;
	private Activity activity;
	
	public OpenGlFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_open_gl, container, false);
		
		this.activity = this.getActivity();
		mGLView = new GLSurfaceView(this.activity);
		mGLView.setRenderer(new OpenGLRenderer());
		LinearLayout modelLayout = (LinearLayout) view.findViewById(R.id.model_layout);
		modelLayout.addView(mGLView);
		
		return view;
	}

}
