package com.mobilesensors.app;

import android.support.v4.view.ViewPager;

/**
 * @author Julius
 * @version $Revision: 1.0 $ Class responsible for listening to pager and its
 *          changes.
 */
public class PagerControl implements ViewPager.OnPageChangeListener {
	/**
	 * Method onPageSelected.
	 * <p>
	 * Indicates which page is currently selected and on that selection does
	 * something.
	 * </p>
	 * 
	 * @param position
	 *            int Indicates which page.
	 */
	@Override
	public void onPageSelected(int position) {
		if (position == 1) {
			
		}
		if (position == 0) {
		}
	}

	/**
	 * Method onPageSelected.
	 * <p>
	 * Indicates which page is scrolled and on that scroll does something.
	 * </p>
	 * 
	 * @param position
	 *            int Indicates which page.
	 */
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
	}

	/**
	 * Method onPageSelected.
	 * <p>
	 * Indicates which page scroll state changed and on that change does
	 * something.
	 * </p>
	 * 
	 * @param state
	 *            int Indicates what state is page scroll in.
	 */
	@Override
	public void onPageScrollStateChanged(int state) {
	}
}
