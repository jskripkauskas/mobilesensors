package com.mobilesensors.app;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.jjoe64.graphview.GraphView.GraphViewData;
import com.viewpagerindicator.TitlePageIndicator;

public class MainActivity extends FragmentActivity implements
		SensorEventListener, OnClickListener {
	// -----------------------------VARIABLES------------------------------------

	/* pager for different graph displaying */
	protected ViewPager pager;

	/* flag to turn sensors on and off */
	private boolean sensorFlag = true;

	/*
	 * pager adapter, responsible for controlling pager
	 */
	private ScreenSlidePagerAdapter pageAdapter;

	/*
	 * constant of how often will the complimentary filter be ran
	 */
	public static final int TIME_CONSTANT = 30;

	/*
	 * filter coefficient, must be adjusted if time_constant is changed
	 */
	public static final float FILTERING_COEFFICIENT = 0.98f;

	/*
	 * timer to run complimentary sensor fusion filter
	 */
	private Timer fusionTimer = new Timer();

	/* final orientation angles from sensor fusion */
	private float[] fusedOrientation = new float[3];
	public static ArrayList<float[]> fusedOrientationGL;

	/* raw gyroscope data */
	private float[] gyroRaw = new float[3];

	/* sensor manager allows to register listeners for sensor data reading */
	private SensorManager sensorManager = null;

	/* accelerometer vector, a.k.a raw data */
	private float[] accelerometer = new float[3];

	/* rotation vector, a.k.a raw data */
	private float[] rotation = new float[3];

	/* magnetic field vector, a.k.a raw data */
	private float[] magnetometer = new float[3];

	/* accelerometer and magnetometer based rotation matrix */
	private float[] rotationMatrix = new float[9];

	/* orientation angles from accelerometer and magnetometer data */
	private float[] accelMagneOrientation = new float[3];

	/* angular speeds from gyroscope (angular speed for each axis) */
	private float[] gyroscope = new float[3];

	/* rotation matrix from gyroscope data */
	private float[] gyroMatrix = new float[9];

	/* orientation angles from gyroscope matrix */
	private float[] gyroOrientation = new float[3];

	public static final float EPSILON = 0.000000001f;

	/*
	 * Constant specifying the factor between a Nano-second and a second
	 */
	private static final float NS2S = 1.0f / 1000000000.0f;
	private float timestamp;

	/* lists of data from each sensor, or sensor fusions */
	private ArrayList<GraphViewData>[] dataListAcc, dataListMag,
			dataListAccMag, dataListGyro, dataListAccMagGyro, dataListRotation,
			dataListFusionCompare, dataListRotationVecotr;

	/*
	 * fragments of graphical representation of data gained from sensors or
	 * sensor fusion
	 */
	private SensorGraph sensorGraphAcc, sensorGraphMag, sensorGraphAccMag,
			sensorGraphGyro, sensorGraphAccMagGyro, sensorGraphRotationVector;
	private DoubleSensorGraph sensorGraphRotation;

	/* fragment for 3D opengl representation of recorded device movement */
	private OpenGlFragment glFragment;
	public static float[][] colorArray;

	/* float filtering variables */
	private boolean dataInit = false;
	private ArrayList<LinkedList<Number>> dataLists = new ArrayList<LinkedList<Number>>();
	private int filterWindow = 30;

	/* Statistics fragment */
	private StatisticsFragment statFragment;

	/* Data lists for statistics */
	private ArrayList<Float> accelZList;
	private ArrayList<Float> magZList;
	private ArrayList<Float> gyroZList;

	/* basic methods for variety of calculations */
	private Formulas formula;

	/* optical flow camera fragment */
	private CameraFragment cameraFragment;

	// -------------------------STATES-------------------------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initCubeColor();
		setUpPager();
		initVariables();

		fusionTimer.scheduleAtFixedRate(new calculateFusedOrientationTask(),
				1000, TIME_CONSTANT);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	// ----------------------------MAIN-FUNCTIONS---------------------------------

	private void initCubeColor() {
		colorArray = new float[6][4];

		for (int d = 0; d < colorArray.length; d++) {
			for (int i = 0; i < colorArray[d].length; i++) {
				colorArray[d][i] = (float) Math.random();
			}
		}
	}

	protected void setUpPager() {
		List<Fragment> fragments = getFragments();
		String[] tempContent;

		tempContent = new String[] { "Camera", "Accelerometer",
				"Magnetometer", "Accelerometer & Magnetometer",
				"Gyroscope", "Rotation Vector", "Combined results",
				"Accelerometer & Magnetometer & Gyroscope",
				"3D movement", "Calculations" };

		pageAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),
				fragments, tempContent);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(pageAdapter);
		TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);
		indicator.setOnPageChangeListener(new PagerControl());
	}

	private List<Fragment> getFragments() {
		List<Fragment> fList = new ArrayList<Fragment>();

		cameraFragment = new CameraFragment();
		fList.add(cameraFragment);

		sensorGraphAcc = new SensorGraph();
		fList.add(sensorGraphAcc);

		sensorGraphMag = new SensorGraph();
		fList.add(sensorGraphMag);

		sensorGraphAccMag = new SensorGraph();
		fList.add(sensorGraphAccMag);

		sensorGraphGyro = new SensorGraph();
		fList.add(sensorGraphGyro);

		sensorGraphRotationVector = new SensorGraph();
		fList.add(sensorGraphRotationVector);

		sensorGraphRotation = new DoubleSensorGraph();
		fList.add(sensorGraphRotation);

		sensorGraphAccMagGyro = new SensorGraph();
		fList.add(sensorGraphAccMagGyro);

		glFragment = new OpenGlFragment();
		fList.add(glFragment);

		statFragment = new StatisticsFragment();
		fList.add(statFragment);

		return fList;
	}

	@SuppressWarnings("unchecked")
	private void initVariables() {
		dataListAcc = new ArrayList[3];
		dataListMag = new ArrayList[3];
		dataListAccMag = new ArrayList[3];
		dataListGyro = new ArrayList[3];
		dataListAccMagGyro = new ArrayList[3];
		dataListRotation = new ArrayList[3];
		dataListFusionCompare = new ArrayList[3];
		dataListRotationVecotr = new ArrayList[3];
		fusedOrientationGL = new ArrayList<float[]>();
		accelZList = new ArrayList<Float>();
		magZList = new ArrayList<Float>();
		gyroZList = new ArrayList<Float>();
		formula = new Formulas();

		findViewById(R.id.buttonSensor).setOnClickListener(this);
	}

	public void initListeners() {
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
				SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			accelerometerSensor(event);
			break;

		case Sensor.TYPE_GYROSCOPE:
			gyroscopeSensor(event);
			break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			magnetometerSensor(event);
			break;

		case Sensor.TYPE_ROTATION_VECTOR:
			rotationSensor(event);
			break;
		}
	}

	public void rotationSensor(SensorEvent event) {
		System.arraycopy(event.values, 0, rotation, 0, 3);

		float[] rotationMat = new float[9];
		SensorManager.getRotationMatrixFromVector(rotationMat, rotation);
		SensorManager.getOrientation(rotationMatrix, rotation);

		// display combined data in graphic
		sensorGraphRotation.updateGraph(dataListFusionCompare,
				dataListRotation, fusedOrientation, rotation);

		// display raw rotation data in graphic
		sensorGraphRotationVector.updateGraph(dataListRotationVecotr, rotation);
	}

	public void accelerometerSensor(SensorEvent event) {
		// copying data to array
		System.arraycopy(event.values, 0, accelerometer, 0, 3);
		// statistics data list
		accelZList.add(accelerometer[2]);
		// filtering data by finding mean
		filterFloat(accelerometer);
		// combine accelerometer and magnetometer
		calculateAccMagOrientation();

		// display raw accelerometer data in graphic
		sensorGraphAcc.updateGraph(dataListAcc, accelerometer);

		// display calculated orientation from accelerometer and
		// magnetometer data in graph
		sensorGraphAccMag.updateGraph(dataListAccMag, accelMagneOrientation);
	}

	public void gyroscopeSensor(SensorEvent event) {
		System.arraycopy(event.values, 0, gyroRaw, 0, 3);
		gyroZList.add(gyroRaw[2]);
		sensorGraphGyro.updateGraph(dataListGyro, gyroRaw);
		gyroFunction(event);
	}

	public void magnetometerSensor(SensorEvent event) {
		System.arraycopy(event.values, 0, magnetometer, 0, 3);
		magZList.add(magnetometer[2]);
		filterFloat(magnetometer);
		sensorGraphMag.updateGraph(dataListMag, magnetometer);
	}

	public void calculateAccMagOrientation() {
		if (SensorManager.getRotationMatrix(rotationMatrix, null,
				accelerometer, magnetometer)) {
			SensorManager.getOrientation(rotationMatrix, accelMagneOrientation);
		}
	}

	// ----------------------------MAIN-FUNCTIONS-FILTERING---------------------------------

	private void getRotationVectorFromGyro(float[] gyroscopeValues,
			float[] rotationChangeVector, float timeFactor) {
		float[] normalisedValues = new float[3];

		// Calculate the angular speed
		float omegaMagnitude = (float) Math.sqrt(gyroscopeValues[0]
				* gyroscopeValues[0] + gyroscopeValues[1] * gyroscopeValues[1]
				+ gyroscopeValues[2] * gyroscopeValues[2]);

		// Normalise the rotation vector
		if (omegaMagnitude > EPSILON) {
			normalisedValues[0] = gyroscopeValues[0] / omegaMagnitude;
			normalisedValues[1] = gyroscopeValues[1] / omegaMagnitude;
			normalisedValues[2] = gyroscopeValues[2] / omegaMagnitude;
		}

		// Rotation change
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		rotationChangeVector[0] = sinThetaOverTwo * normalisedValues[0];
		rotationChangeVector[1] = sinThetaOverTwo * normalisedValues[1];
		rotationChangeVector[2] = sinThetaOverTwo * normalisedValues[2];
		rotationChangeVector[3] = cosThetaOverTwo;
	}

	public float[] filterFloat(float[] data) {
		for (int i = 0; i < data.length; i++) {
			// Initialize the data structures for the data set.
			if (!dataInit) {
				dataLists.add(new LinkedList<Number>());
			}

			dataLists.get(i).addLast(data[i]);

			if (dataLists.get(i).size() > filterWindow) {
				dataLists.get(i).removeFirst();
			}
		}

		dataInit = true;
		float[] means = new float[dataLists.size()];

		for (int i = 0; i < dataLists.size(); i++) {
			means[i] = (float) getMean(dataLists.get(i));
		}

		return means;
	}

	private float getMean(List<Number> data) {
		float m = 0;
		float count = 0;

		for (int i = 0; i < data.size(); i++) {
			m += data.get(i).floatValue();
			count++;
		}

		if (count != 0) {
			m = m / count;
		}

		return m;
	}

	public void gyroFunction(SensorEvent event) {
		if (accelMagneOrientation == null)
			return;

		// convert the raw gyroscope data into a rotation vector
		float[] changeVector = new float[4];
		if (timestamp != 0) {
			final float dT = (event.timestamp - timestamp) * NS2S;
			System.arraycopy(event.values, 0, gyroscope, 0, 3);
			getRotationVectorFromGyro(gyroscope, changeVector, dT / 2.0f);
		}

		// save current time stamp for next interval
		timestamp = event.timestamp;

		// convert rotation vector into rotation matrix
		float[] changeMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(changeMatrix, changeVector);

		// apply the new rotation interval on the gyroscope
		gyroMatrix = formula.matrixMultiplication(gyroMatrix, changeMatrix);

		// get the gyroscope based orientation from the rotation matrix
		SensorManager.getOrientation(gyroMatrix, gyroOrientation);

		/*
		 * already filtered and combined data shown in graph, call is there and
		 * not in timer to avoid calling UI functions from non-main thread
		 */
		sensorGraphAccMagGyro.updateGraph(dataListAccMagGyro, fusedOrientation);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	// ---------------------INNER-CLASSES--------------------------------------

	class calculateFusedOrientationTask extends TimerTask {

		/* minus 180� */
		public float[] minusAngle(float[] fusion, int index) {
			if (fusion[index] > Math.PI) {
				fusion[index] -= 2.0 * Math.PI;
			}

			return fusion;
		}

		/* plus 180� */
		public float plusAngle(float[] fusion, int index) {
			fusion[index] += 2.0 * Math.PI;
			return fusion[index];
		}

		public void fixTransition() {
			float oneMinusCoeff = (1.0f - FILTERING_COEFFICIENT);

			/*
			 * Fix for 179� to -179� transition.
			 */

			// X
			if (gyroOrientation[0] < -0.5 * Math.PI
					&& accelMagneOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTERING_COEFFICIENT
						* plusAngle(gyroOrientation, 0) + oneMinusCoeff
						* accelMagneOrientation[0]);
				minusAngle(fusedOrientation, 0);
			} else if (accelMagneOrientation[0] < -0.5 * Math.PI
					&& gyroOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTERING_COEFFICIENT
						* gyroOrientation[0] + oneMinusCoeff
						* plusAngle(accelMagneOrientation, 0));
				minusAngle(fusedOrientation, 0);
			} else {
				fusedOrientation[0] = FILTERING_COEFFICIENT
						* gyroOrientation[0] + oneMinusCoeff
						* accelMagneOrientation[0];
			}

			// Y
			if (gyroOrientation[1] < -0.5 * Math.PI
					&& accelMagneOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTERING_COEFFICIENT
						* plusAngle(gyroOrientation, 1) + oneMinusCoeff
						* accelMagneOrientation[1]);
				minusAngle(fusedOrientation, 1);
			} else if (accelMagneOrientation[1] < -0.5 * Math.PI
					&& gyroOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTERING_COEFFICIENT
						* gyroOrientation[1] + oneMinusCoeff
						* plusAngle(accelMagneOrientation, 1));
				minusAngle(fusedOrientation, 1);
			} else {
				fusedOrientation[1] = FILTERING_COEFFICIENT
						* gyroOrientation[1] + oneMinusCoeff
						* accelMagneOrientation[1];
			}

			// Z
			if (gyroOrientation[2] < -0.5 * Math.PI
					&& accelMagneOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTERING_COEFFICIENT
						* plusAngle(gyroOrientation, 2) + oneMinusCoeff
						* accelMagneOrientation[2]);
				minusAngle(fusedOrientation, 2);
			} else if (accelMagneOrientation[2] < -0.5 * Math.PI
					&& gyroOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTERING_COEFFICIENT
						* gyroOrientation[2] + oneMinusCoeff
						* plusAngle(accelMagneOrientation, 2));
				minusAngle(fusedOrientation, 2);
			} else {
				fusedOrientation[2] = FILTERING_COEFFICIENT
						* gyroOrientation[2] + oneMinusCoeff
						* accelMagneOrientation[2];
			}
		}

		public void run() {
			fixTransition();

			// overwrite gyroscope matrix and orientation with fused orientation
			// to compensate gyroscope drift
			gyroMatrix = formula
					.getRotationMatrixFromOrientation(fusedOrientation);
			System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);

			float[] quaternion = formula
					.getQuaternionFromRotationMatrix(gyroMatrix);
			fusedOrientationGL.add(quaternion);
		}
	}

	// -------------------BUTTON-LISTENERS------------------------------------

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.buttonSensor) {
			Button sensorBtn = (Button) findViewById(R.id.buttonSensor);

			if (sensorFlag) {
				turnSensorOn(sensorBtn);
				sensorFlag = !sensorFlag;
			} else {
				turnSensorOff(sensorBtn);
				sensorFlag = !sensorFlag;
			}
		}
	}

	private void turnSensorOn(Button sensorBtn) {
		for (int i = 0; i < 3; i++) {
			dataListAcc[i] = new ArrayList<GraphViewData>();
			dataListMag[i] = new ArrayList<GraphViewData>();
			dataListAccMag[i] = new ArrayList<GraphViewData>();
			dataListGyro[i] = new ArrayList<GraphViewData>();
			dataListAccMagGyro[i] = new ArrayList<GraphViewData>();
			dataListRotation[i] = new ArrayList<GraphViewData>();
			dataListFusionCompare[i] = new ArrayList<GraphViewData>();
			dataListRotationVecotr[i] = new ArrayList<GraphViewData>();
		}

		cameraFragment.setOpticalFlowFlag(true);
		sensorBtn.setText("Sensor On");
		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
		initListeners();
	}

	public void turnSensorOff(Button sensorBtn) {
		sensorBtn.setText("Sensor Off");
		cameraFragment.setOpticalFlowFlag(false);
		sensorManager.unregisterListener(this);
	}

	// -----------------------------GETTERS-SETTERS----------------------------------

	public ArrayList<Float> getAccelZList() {
		return accelZList;
	}

	public void setAccelZList(ArrayList<Float> accelZList) {
		this.accelZList = accelZList;
	}

	public ArrayList<Float> getMagZList() {
		return magZList;
	}

	public void setMagZList(ArrayList<Float> magZList) {
		this.magZList = magZList;
	}

	public ArrayList<Float> getGyroZList() {
		return gyroZList;
	}

	public void setGyroZList(ArrayList<Float> gyroZList) {
		this.gyroZList = gyroZList;
	}
}
