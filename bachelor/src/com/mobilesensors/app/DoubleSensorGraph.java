package com.mobilesensors.app;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;

public class DoubleSensorGraph extends Fragment {
	// --------------------------------VARIABLES-----------------------------------

	private Long timestamp = 0l;
	private LineGraphView graphView;
	private int count = 0;
	private GraphViewData[] dataX, dataY, dataZ, dataX2, dataY2, dataZ2;
	private Activity activity;
	private LinearLayout layout;

	public DoubleSensorGraph() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.activity = this.getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		this.layout = (LinearLayout) view
				.findViewById(R.id.sensor_graph_layout);
		return view;
	}

	// ----------------------------MAIN-FUNCTIONS------------------------------

	public void updateGraph(ArrayList<GraphViewData>[] dataList,
			ArrayList<GraphViewData>[] dataList2, float[] data, float[] data2) {
		if (this.isAdded()) {
			Long diff = System.nanoTime() - timestamp;

			if (diff > 10000000) {
				timestamp = System.nanoTime();
				this.layout.removeAllViews();

				for (int i = 0; i < dataList.length; i++) {
					dataList[i].add(new GraphViewData(count, data[i]));
				}

				for (int d = 0; d < dataList2.length; d++) {
					dataList2[d].add(new GraphViewData(count, data2[d]));
				}
				count++;

				GraphViewSeriesStyle styleX = new GraphViewSeriesStyle();
				styleX.color = Color.rgb(200, 50, 00);

				GraphViewSeriesStyle styleY = new GraphViewSeriesStyle();
				styleY.color = Color.rgb(90, 200, 00);

				GraphViewSeriesStyle styleX2 = new GraphViewSeriesStyle();
				styleX2.color = Color.rgb(100, 30, 20);

				GraphViewSeriesStyle styleY2 = new GraphViewSeriesStyle();
				styleY2.color = Color.rgb(90, 150, 00);

				GraphViewSeriesStyle styleZ2 = new GraphViewSeriesStyle();
				styleZ2.color = Color.rgb(150, 150, 100);

				dataX = new GraphViewData[dataList[0].size()];
				dataY = new GraphViewData[dataList[1].size()];
				dataZ = new GraphViewData[dataList[2].size()];

				dataX2 = new GraphViewData[dataList2[0].size()];
				dataY2 = new GraphViewData[dataList2[1].size()];
				dataZ2 = new GraphViewData[dataList2[2].size()];

				for (int i = 0; i < dataList[0].size(); i++) {
					dataX[i] = dataList[0].get(i);
					dataY[i] = dataList[1].get(i);
					dataZ[i] = dataList[2].get(i);
				}

				for (int d = 0; d < dataList2[0].size(); d++) {
					dataX2[d] = dataList2[0].get(d);
					dataY2[d] = dataList2[1].get(d);
					dataZ2[d] = dataList2[2].get(d);
				}

				GraphViewSeries seriesX = new GraphViewSeries("X SF axis",
						styleX, dataX);
				GraphViewSeries seriesY = new GraphViewSeries("Y SF axis",
						styleY, dataY);
				GraphViewSeries seriesZ = new GraphViewSeries("Z SF axis",
						null, dataZ);

				GraphViewSeries seriesX2 = new GraphViewSeries("X R axis",
						styleX2, dataX2);
				GraphViewSeries seriesY2 = new GraphViewSeries("Y R axis",
						styleY2, dataY2);
				GraphViewSeries seriesZ2 = new GraphViewSeries("Z R axis",
						styleZ2, dataZ2);

				graphView = new LineGraphView(activity, "Sensor graph");

				graphView.addSeries(seriesX);
				graphView.addSeries(seriesY);
				graphView.addSeries(seriesZ);
				graphView.addSeries(seriesX2);
				graphView.addSeries(seriesY2);
				graphView.addSeries(seriesZ2);
				graphView.setScalable(true);
				graphView.setShowLegend(true);

				this.layout.addView(graphView);
			}
		}
	}
}
