package com.mobilesensors.app;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

public class CameraFragment extends Fragment implements CvCameraViewListener {

	private Point pt, pt2;

	private int x, y, iLineThickness = 3, iGFFTMax = 40;

	private JavaCameraView openCvCameraView;

	private List<Byte> byteStatus;
	private List<Point> cornersThis, cornersPrev;

	private Mat mRgba, matOpFlowPrev, matOpFlowThis;

	private MatOfFloat mMOFerr;
	private MatOfByte mMOBStatus;
	private MatOfPoint2f mMOP2fptsPrev, mMOP2fptsThis, mMOP2fptsSafe;
	private MatOfPoint MOPcorners;

	private Scalar colorRed;
	private Size sMatSize;

	private View view;
	private boolean opticalFlowFlag = false;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(
			this.getActivity()) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
				openCvCameraView.enableView();
				break;
			default:
				super.onManagerConnected(status);
				break;
			}
		}
	};
	

	static {
	    if (!OpenCVLoader.initDebug()) {
	        // Handle initialization error
	    }
	}

	public CameraFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.fragment_camera, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		openCvCameraView = (JavaCameraView) view.findViewById(R.id.cameraView);
		openCvCameraView.setCvCameraViewListener(this);
		openCvCameraView.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (openCvCameraView != null)
			openCvCameraView.disableView();
	}

	public void onResume() {
		super.onResume();
	}

	public void onDestroy() {
		super.onDestroy();
		if (openCvCameraView != null)
			openCvCameraView.disableView();
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
		byteStatus = new ArrayList<Byte>();

		colorRed = new Scalar(255, 0, 0, 255);
		cornersThis = new ArrayList<Point>();
		cornersPrev = new ArrayList<Point>();

		mMOP2fptsPrev = new MatOfPoint2f();
		mMOP2fptsThis = new MatOfPoint2f();
		mMOP2fptsSafe = new MatOfPoint2f();
		mMOFerr = new MatOfFloat();
		mMOBStatus = new MatOfByte();
		MOPcorners = new MatOfPoint();
		mRgba = new Mat();
		matOpFlowThis = new Mat();
		matOpFlowPrev = new Mat();

		pt = new Point(0, 0);
		pt2 = new Point(0, 0);

		sMatSize = new Size();

		mRgba = new Mat(height, width, CvType.CV_8UC4);
	}

	@Override
	public void onCameraViewStopped() {
		mRgba.release();
		MOPcorners.release();
	}

	@Override
	public Mat onCameraFrame(Mat inputFrame) {
		inputFrame.copyTo(mRgba);
		sMatSize.width = mRgba.width();
		sMatSize.height = mRgba.height();

		if (mMOP2fptsPrev.rows() == 0) {

			// first time through the loop so we need prev and this mats
			// plus prev points
			// get this mat
			Imgproc.cvtColor(mRgba, matOpFlowThis, Imgproc.COLOR_RGBA2GRAY);

			// copy that to prev mat
			matOpFlowThis.copyTo(matOpFlowPrev);

			// get prev corners
			Imgproc.goodFeaturesToTrack(matOpFlowPrev, MOPcorners, iGFFTMax,
					0.05, 20);
			mMOP2fptsPrev.fromArray(MOPcorners.toArray());

			// get safe copy of this corners
			mMOP2fptsPrev.copyTo(mMOP2fptsSafe);
		} else {
			// we've been through before so
			// this mat is valid. Copy it to prev mat
			matOpFlowThis.copyTo(matOpFlowPrev);

			// get this mat
			Imgproc.cvtColor(mRgba, matOpFlowThis, Imgproc.COLOR_RGBA2GRAY);

			// get the corners for this mat
			Imgproc.goodFeaturesToTrack(matOpFlowThis, MOPcorners, iGFFTMax,
					0.05, 20);
			mMOP2fptsThis.fromArray(MOPcorners.toArray());

			// retrieve the corners from the prev mat
			// (saves calculating them again)
			mMOP2fptsSafe.copyTo(mMOP2fptsPrev);

			// and save this corners for next time through

			mMOP2fptsThis.copyTo(mMOP2fptsSafe);
		}

		if (opticalFlowFlag) {
			Video.calcOpticalFlowPyrLK(matOpFlowPrev, matOpFlowThis,
					mMOP2fptsPrev, mMOP2fptsThis, mMOBStatus, mMOFerr);

			cornersPrev = mMOP2fptsPrev.toList();
			cornersThis = mMOP2fptsThis.toList();
			byteStatus = mMOBStatus.toList();

			y = byteStatus.size() - 1;

			for (x = 0; x < y; x++) {
				if (byteStatus.get(x) == 1) {
					pt = cornersThis.get(x);
					pt2 = cornersPrev.get(x);

					Core.circle(mRgba, pt, 5, colorRed, iLineThickness - 1);
					Core.line(mRgba, pt, pt2, colorRed, iLineThickness);
				}
			}
		}

		Core.flip(mRgba.t(), mRgba.t(), 1);

		return mRgba;
	}

	public boolean isOpticalFlowFlag() {
		return opticalFlowFlag;
	}

	public void setOpticalFlowFlag(boolean opticalFlowFlag) {
		this.opticalFlowFlag = opticalFlowFlag;
	}

}
