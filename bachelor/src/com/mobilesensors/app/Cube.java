package com.mobilesensors.app;

import javax.microedition.khronos.opengles.GL10;

public class Cube {

	public Square square;
	private float[][] squareColor;

	public Cube() {

		square = new Square();
		squareColor = MainActivity.colorArray;
	}

	public void draw(GL10 gl) {

		gl.glPushMatrix();
		gl.glTranslatef(0, 0, 1);

		// front surface
		square.draw(gl, squareColor[0]);

		// front surface
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, -1);
		gl.glTranslatef(1, 0, 0);
		gl.glRotatef(90, 0, 1, 0);
		square.draw(gl, squareColor[1]);
		gl.glPopMatrix();

		// back surface
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, -2);
		square.draw(gl, squareColor[2]);
		gl.glPopMatrix();

		// left surface
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, -1);
		gl.glTranslatef(-1, 0, 0);
		gl.glRotatef(90, 0, 1, 0);
		square.draw(gl, squareColor[3]);
		gl.glPopMatrix();

		// top surface
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, -1);
		gl.glTranslatef(0, 1, 0);
		gl.glRotatef(90, 1, 0, 0);
		square.draw(gl, squareColor[4]);
		gl.glPopMatrix();

		// bottom surface
		gl.glPushMatrix();
		gl.glTranslatef(0, 0, -1);
		gl.glTranslatef(0, -1, 0);
		gl.glRotatef(90, 1, 0, 0);
		square.draw(gl, squareColor[5]);
		gl.glPopMatrix();

		gl.glPopMatrix();

	}
}