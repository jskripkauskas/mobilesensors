package com.mobilesensors.app;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class StatisticsFragment extends Fragment implements OnClickListener {
	private View view;
	private MainActivity activity;
	private Formulas formula;

	public static StatisticsFragment newInstance(String param1, String param2) {
		StatisticsFragment fragment = new StatisticsFragment();
		return fragment;
	}

	public StatisticsFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.activity = (MainActivity) this.getActivity();
		formula = new Formulas();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_statistics, container, false);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		view.findViewById(R.id.enableStatistics).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.enableStatistics:
			displayAccelerometerCalculations();
			displayMagnetometerCalculations();
			displayGyroscopeCalculations();
		}
	}

	private void displayGyroscopeCalculations() {
		TextView gyroAverageText, gyroMinText, gyroMaxText, gyroDevText;
		gyroAverageText = (TextView) view.findViewById(R.id.gyroAverage);
		gyroMinText = (TextView) view.findViewById(R.id.gyroMin);
		gyroMaxText = (TextView) view.findViewById(R.id.gyroMax);
		gyroDevText = (TextView) view.findViewById(R.id.gyroDev);
		ArrayList<Float> gyro = activity.getGyroZList();
		displayCalculations(gyroAverageText, gyroMinText, gyroMaxText,
				gyroDevText, gyro);
	}

	private void displayMagnetometerCalculations() {
		TextView magAverageText, magMinText, magMaxText, magDevText;
		magAverageText = (TextView) view.findViewById(R.id.magAverage);
		magMinText = (TextView) view.findViewById(R.id.magMin);
		magMaxText = (TextView) view.findViewById(R.id.magMax);
		magDevText = (TextView) view.findViewById(R.id.magDev);
		ArrayList<Float> mag = activity.getMagZList();
		displayCalculations(magAverageText, magMinText, magMaxText, magDevText,
				mag);
	}

	private void displayAccelerometerCalculations() {
		TextView accelAverageText, accelMinText, accelMaxText, accelDevText;
		accelAverageText = (TextView) view.findViewById(R.id.accelAverage);
		accelMinText = (TextView) view.findViewById(R.id.accelMin);
		accelMaxText = (TextView) view.findViewById(R.id.accelMax);
		accelDevText = (TextView) view.findViewById(R.id.accelDev);
		ArrayList<Float> accel = activity.getAccelZList();
		displayCalculations(accelAverageText, accelMinText, accelMaxText,
				accelDevText, accel);
	}

	private void displayCalculations(TextView avgView, TextView minView,
			TextView maxView, TextView devView, ArrayList<Float> data) {
		float average = formula.calculateAverage(data);
		float min = formula.calculateMin(data);
		float max = formula.calculateMax(data);
		float deviation = max - min;

		avgView.setText(average + "");
		minView.setText(min + "");
		maxView.setText(max + "");
		devView.setText(deviation + "");
	}

}
