package com.mobilesensors.app;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;

public class SensorGraph extends Fragment {
	// --------------------------------VARIABLES-----------------------------------

	private Long timestamp = 0l;
	private LineGraphView graphView;
	private int count = 0;
	private GraphViewData[] dataX, dataY, dataZ;
	private Activity activity;
	private LinearLayout layout;

	public SensorGraph() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.activity = this.getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		this.layout = (LinearLayout) view
				.findViewById(R.id.sensor_graph_layout);
		return view;
	}

	// ----------------------------MAIN-FUNCTIONS------------------------------

	public void updateGraph(ArrayList<GraphViewData>[] dataList, float[] data) {
		if (this.isAdded()) {
			Long diff = System.nanoTime() - timestamp;

			if (diff > 10000000) {
				timestamp = System.nanoTime();
				 this.layout.removeAllViews();

				for (int i = 0; i < dataList.length; i++) {
					dataList[i].add(new GraphViewData(count, data[i]));
				}
				count++;

				GraphViewSeriesStyle styleX = new GraphViewSeriesStyle();
				styleX.color = Color.rgb(200, 50, 00);

				GraphViewSeriesStyle styleY = new GraphViewSeriesStyle();
				styleY.color = Color.rgb(90, 250, 00);

				dataX = new GraphViewData[dataList[0].size()];
				dataY = new GraphViewData[dataList[1].size()];
				dataZ = new GraphViewData[dataList[2].size()];

				for (int i = 0; i < dataList[0].size(); i++) {
					dataX[i] = dataList[0].get(i);
					dataY[i] = dataList[1].get(i);
					dataZ[i] = dataList[2].get(i);
				}

				GraphViewSeries seriesX = new GraphViewSeries("X axis", styleX,
						dataX);
				GraphViewSeries seriesY = new GraphViewSeries("Y axis", styleY,
						dataY);
				GraphViewSeries seriesZ = new GraphViewSeries("Z axis", null,
						dataZ);

				graphView = new LineGraphView(activity, "Sensor graph");

				graphView.addSeries(seriesX);
				graphView.addSeries(seriesY);
				graphView.addSeries(seriesZ);
				graphView.setScalable(true);
				graphView.setShowLegend(true);

				this.layout.addView(graphView);
			}
		}
	}
}
